﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.rtbDatos = New System.Windows.Forms.RichTextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.pRich = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pBoton = New System.Windows.Forms.Panel()
        Me.pDerecha = New System.Windows.Forms.Panel()
        Me.lbOperadores = New System.Windows.Forms.ListBox()
        Me.lbOp = New System.Windows.Forms.ListBox()
        Me.lbReservadas = New System.Windows.Forms.ListBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rcExpresionRegular = New System.Windows.Forms.RichTextBox()
        Me.NoReconocido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Operador = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Digito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OperadorRelacional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Preservada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Token = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Linea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvDatos = New System.Windows.Forms.DataGridView()
        Me.pIzquierda = New System.Windows.Forms.Panel()
        Me.pFondo = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TipoDato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbErrores = New System.Windows.Forms.ListBox()
        Me.Panel4.SuspendLayout()
        Me.pRich.SuspendLayout()
        Me.pBoton.SuspendLayout()
        Me.pDerecha.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pIzquierda.SuspendLayout()
        Me.pFondo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(12, 531)
        Me.Panel3.TabIndex = 0
        '
        'rtbDatos
        '
        Me.rtbDatos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbDatos.Dock = System.Windows.Forms.DockStyle.Top
        Me.rtbDatos.Location = New System.Drawing.Point(0, 13)
        Me.rtbDatos.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.rtbDatos.Name = "rtbDatos"
        Me.rtbDatos.Size = New System.Drawing.Size(419, 290)
        Me.rtbDatos.TabIndex = 6
        Me.rtbDatos.Text = "if a > bb then" & Global.Microsoft.VisualBasic.ChrW(10) & "msgbox(""gano"")" & Global.Microsoft.VisualBasic.ChrW(10) & "else" & Global.Microsoft.VisualBasic.ChrW(10) & "msgbox(""no gano"")" & Global.Microsoft.VisualBasic.ChrW(10) & "x = 5 + 3"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lbErrores)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.rtbDatos)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(12, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(419, 531)
        Me.Panel4.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(419, 13)
        Me.Panel5.TabIndex = 5
        '
        'pRich
        '
        Me.pRich.Controls.Add(Me.Panel4)
        Me.pRich.Controls.Add(Me.Panel3)
        Me.pRich.Dock = System.Windows.Forms.DockStyle.Top
        Me.pRich.Location = New System.Drawing.Point(0, 0)
        Me.pRich.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pRich.Name = "pRich"
        Me.pRich.Size = New System.Drawing.Size(431, 531)
        Me.pRich.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Teal
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(0, 0)
        Me.Button1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(431, 45)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "PROCESAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'pBoton
        '
        Me.pBoton.Controls.Add(Me.Button1)
        Me.pBoton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pBoton.Location = New System.Drawing.Point(0, 531)
        Me.pBoton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pBoton.Name = "pBoton"
        Me.pBoton.Size = New System.Drawing.Size(431, 45)
        Me.pBoton.TabIndex = 1
        '
        'pDerecha
        '
        Me.pDerecha.Controls.Add(Me.pBoton)
        Me.pDerecha.Controls.Add(Me.pRich)
        Me.pDerecha.Dock = System.Windows.Forms.DockStyle.Right
        Me.pDerecha.Location = New System.Drawing.Point(907, 0)
        Me.pDerecha.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pDerecha.Name = "pDerecha"
        Me.pDerecha.Size = New System.Drawing.Size(431, 576)
        Me.pDerecha.TabIndex = 0
        '
        'lbOperadores
        '
        Me.lbOperadores.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbOperadores.FormattingEnabled = True
        Me.lbOperadores.ItemHeight = 16
        Me.lbOperadores.Items.AddRange(New Object() {"<", ">", "<=", ">=", "=", "<>", "!="})
        Me.lbOperadores.Location = New System.Drawing.Point(155, 0)
        Me.lbOperadores.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lbOperadores.Name = "lbOperadores"
        Me.lbOperadores.Size = New System.Drawing.Size(47, 40)
        Me.lbOperadores.TabIndex = 13
        '
        'lbOp
        '
        Me.lbOp.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbOp.FormattingEnabled = True
        Me.lbOp.ItemHeight = 16
        Me.lbOp.Items.AddRange(New Object() {"+", "*", "\", "/", "MOD", "-", "^"})
        Me.lbOp.Location = New System.Drawing.Point(109, 0)
        Me.lbOp.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lbOp.Name = "lbOp"
        Me.lbOp.Size = New System.Drawing.Size(46, 40)
        Me.lbOp.TabIndex = 14
        '
        'lbReservadas
        '
        Me.lbReservadas.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbReservadas.Dock = System.Windows.Forms.DockStyle.Left
        Me.lbReservadas.FormattingEnabled = True
        Me.lbReservadas.ItemHeight = 16
        Me.lbReservadas.Items.AddRange(New Object() {"AddHandler", "AddressOf", "Alias", "And", "AndAlso", "As", "Boolean", "ByRef", "Byte", "ByVal", "Call", "Case", "Catch", "cByte", "CBool", "CChar", "CDate", "CDec", "Char", "CDbl", "CInt", "Class", "CLng", "Cobj", "Const", "Continue", "CSByte", "CShort", "CSng", "CStr", "CType", "CUInt", "CULng", "CUShort", "Date", "Decimal", "Declare", "Default", "Delegate", "Dim", "Do", "Double", "DirectCast", "Each", "Else", "ElseIf", "End", "EndIf", "Enum", "Erase", "Error", "Event", "Exit", "False", "Finally", "For", "Friend", "Function", "Get", "GetType", "Global", "Gosub", "GoTo", "Handles", "If", "Implement", "Import", "In", "Inherits", "Integer", "Interface", "Is", "IsNot", "Let", "Lib", "Like", "Long", "Loop", "Me", "Mod", "Module", "MustInherit", "MustOverride", "MyBase", "MyClass", "Namespace", "New", "Next", "Not", "Nothing", "NotOverridable", "Object", "Of", "On", "Option", "Optional", "Operator", "Or", "OrElse", "Overloads", "Overridable", "Overrides", "Partial", "ParamArray", "Private", "Property", "Protected", "Public", "RaiseEvent", "ReadOnly", "ReDim", "REM", "RemoveHandler", "Resume", "Return", "SByte", "Select", "Set", "Shadows", "Short", "Single", "Shared", "Static", "Step", "Stop", "String", "Sub", "Structure", "SyncLock", "then", "To", "True", "Try", "Throw", "TypeOf", "Wend", "Variant", "TypeOf", "UInteger", "ULong", "UShort", "Using", "When", "While", "With", "WithEvents", "WriteOnly", "Xor"})
        Me.lbReservadas.Location = New System.Drawing.Point(0, 0)
        Me.lbReservadas.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lbReservadas.Name = "lbReservadas"
        Me.lbReservadas.Size = New System.Drawing.Size(109, 40)
        Me.lbReservadas.TabIndex = 12
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.rcExpresionRegular)
        Me.Panel1.Controls.Add(Me.lbOperadores)
        Me.Panel1.Controls.Add(Me.lbOp)
        Me.Panel1.Controls.Add(Me.lbReservadas)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 536)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(907, 40)
        Me.Panel1.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Teal
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(469, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(128, 40)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "RESERVADAS"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label1.Location = New System.Drawing.Point(597, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 17)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Ex Reg: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'rcExpresionRegular
        '
        Me.rcExpresionRegular.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rcExpresionRegular.Dock = System.Windows.Forms.DockStyle.Right
        Me.rcExpresionRegular.Location = New System.Drawing.Point(658, 0)
        Me.rcExpresionRegular.Name = "rcExpresionRegular"
        Me.rcExpresionRegular.Size = New System.Drawing.Size(249, 40)
        Me.rcExpresionRegular.TabIndex = 15
        Me.rcExpresionRegular.Text = "^[a-zA-Z-]*$"
        '
        'NoReconocido
        '
        Me.NoReconocido.HeaderText = "No Reconocido"
        Me.NoReconocido.MinimumWidth = 6
        Me.NoReconocido.Name = "NoReconocido"
        Me.NoReconocido.ReadOnly = True
        '
        'Operador
        '
        Me.Operador.HeaderText = "Operador"
        Me.Operador.MinimumWidth = 6
        Me.Operador.Name = "Operador"
        Me.Operador.ReadOnly = True
        '
        'Digito
        '
        Me.Digito.HeaderText = "Dígito"
        Me.Digito.MinimumWidth = 6
        Me.Digito.Name = "Digito"
        Me.Digito.ReadOnly = True
        '
        'OperadorRelacional
        '
        Me.OperadorRelacional.HeaderText = "Operador Relacional"
        Me.OperadorRelacional.MinimumWidth = 6
        Me.OperadorRelacional.Name = "OperadorRelacional"
        Me.OperadorRelacional.ReadOnly = True
        '
        'Id
        '
        Me.Id.HeaderText = "ID"
        Me.Id.MinimumWidth = 6
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        '
        'Preservada
        '
        Me.Preservada.HeaderText = "Palabra Reservada"
        Me.Preservada.MinimumWidth = 6
        Me.Preservada.Name = "Preservada"
        Me.Preservada.ReadOnly = True
        '
        'Token
        '
        Me.Token.HeaderText = "Token"
        Me.Token.MinimumWidth = 6
        Me.Token.Name = "Token"
        Me.Token.ReadOnly = True
        '
        'Linea
        '
        Me.Linea.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Linea.HeaderText = "Línea"
        Me.Linea.MinimumWidth = 6
        Me.Linea.Name = "Linea"
        Me.Linea.ReadOnly = True
        Me.Linea.Width = 72
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvDatos)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(907, 536)
        Me.Panel2.TabIndex = 1
        '
        'dgvDatos
        '
        Me.dgvDatos.AllowUserToAddRows = False
        Me.dgvDatos.AllowUserToDeleteRows = False
        Me.dgvDatos.AllowUserToResizeColumns = False
        Me.dgvDatos.AllowUserToResizeRows = False
        Me.dgvDatos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDatos.BackgroundColor = System.Drawing.Color.White
        Me.dgvDatos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDatos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvDatos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDatos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Linea, Me.Token, Me.Preservada, Me.Id, Me.OperadorRelacional, Me.Digito, Me.Operador, Me.NoReconocido, Me.TipoDato})
        Me.dgvDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDatos.Location = New System.Drawing.Point(0, 0)
        Me.dgvDatos.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgvDatos.MultiSelect = False
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.ReadOnly = True
        Me.dgvDatos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvDatos.RowHeadersVisible = False
        Me.dgvDatos.RowHeadersWidth = 51
        Me.dgvDatos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDatos.RowTemplate.Height = 28
        Me.dgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDatos.Size = New System.Drawing.Size(907, 536)
        Me.dgvDatos.TabIndex = 8
        '
        'pIzquierda
        '
        Me.pIzquierda.Controls.Add(Me.Panel2)
        Me.pIzquierda.Controls.Add(Me.Panel1)
        Me.pIzquierda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pIzquierda.Location = New System.Drawing.Point(0, 0)
        Me.pIzquierda.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pIzquierda.Name = "pIzquierda"
        Me.pIzquierda.Size = New System.Drawing.Size(907, 576)
        Me.pIzquierda.TabIndex = 1
        '
        'pFondo
        '
        Me.pFondo.Controls.Add(Me.pIzquierda)
        Me.pFondo.Controls.Add(Me.pDerecha)
        Me.pFondo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pFondo.Location = New System.Drawing.Point(0, 0)
        Me.pFondo.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pFondo.Name = "pFondo"
        Me.pFondo.Size = New System.Drawing.Size(1338, 576)
        Me.pFondo.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(450, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 29)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "|"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Teal
        Me.Button3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(383, 0)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(67, 40)
        Me.Button3.TabIndex = 19
        Me.Button3.Text = "SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TipoDato
        '
        Me.TipoDato.HeaderText = "Tipo de Dato"
        Me.TipoDato.Name = "TipoDato"
        Me.TipoDato.ReadOnly = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(0, 303)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 25)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "ERRORES"
        '
        'lbErrores
        '
        Me.lbErrores.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbErrores.FormattingEnabled = True
        Me.lbErrores.ItemHeight = 16
        Me.lbErrores.Location = New System.Drawing.Point(0, 328)
        Me.lbErrores.Name = "lbErrores"
        Me.lbErrores.Size = New System.Drawing.Size(419, 203)
        Me.lbErrores.TabIndex = 8
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1338, 576)
        Me.Controls.Add(Me.pFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.pRich.ResumeLayout(False)
        Me.pBoton.ResumeLayout(False)
        Me.pDerecha.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pIzquierda.ResumeLayout(False)
        Me.pFondo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel3 As Panel
    Friend WithEvents rtbDatos As RichTextBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents pRich As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents pBoton As Panel
    Friend WithEvents pDerecha As Panel
    Friend WithEvents lbOperadores As ListBox
    Friend WithEvents lbOp As ListBox
    Friend WithEvents lbReservadas As ListBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents NoReconocido As DataGridViewTextBoxColumn
    Friend WithEvents Operador As DataGridViewTextBoxColumn
    Friend WithEvents Digito As DataGridViewTextBoxColumn
    Friend WithEvents OperadorRelacional As DataGridViewTextBoxColumn
    Friend WithEvents Id As DataGridViewTextBoxColumn
    Friend WithEvents Preservada As DataGridViewTextBoxColumn
    Friend WithEvents Token As DataGridViewTextBoxColumn
    Friend WithEvents Linea As DataGridViewTextBoxColumn
    Friend WithEvents Panel2 As Panel
    Friend WithEvents dgvDatos As DataGridView
    Friend WithEvents pIzquierda As Panel
    Friend WithEvents pFondo As Panel
    Friend WithEvents rcExpresionRegular As RichTextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents TipoDato As DataGridViewTextBoxColumn
    Friend WithEvents Label3 As Label
    Friend WithEvents lbErrores As ListBox
End Class
