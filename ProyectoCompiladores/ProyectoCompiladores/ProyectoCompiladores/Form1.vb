﻿Imports System.Text.RegularExpressions
Public Class Form1
    Dim c As Integer = 0
    Dim l As Integer = 0
    Public Function validarAbecedario(ByVal abc As String) As Boolean
        ' retorna true o false   
        Return Regex.IsMatch(abc, rcExpresionRegular.Text)
    End Function
    Private Sub agregarDatos()
        'declaro un array que recibe las líneas del richtextbox y splitea cuando encuentra un enter
        Dim arrayRich() As String = Strings.Split(rtbDatos.Text, vbLf)
        'declaro mis delimitadores
        'Dim delimitadores() As String = {" ", ".", "(", ")", "'", ChrW(34), vbLf}
        Dim delimitadores() As String = {" ", vbLf}
        'declaro un vector auxiliar que va a splitear cada vez que encuentre 
        'delimitadores declarados anteriormente
        Dim vectorAux() As String

        'Recorro línea por línea el richtext
        For i As Integer = 0 To arrayRich.Length - 1
            'paso a la variable texto el arrayrich, iterando cada una de sus lineas
            If arrayRich(i).StartsWith("/") Then
                'MessageBox.Show("Esto es un comentario")
            ElseIf arrayRich(i).StartsWith("/*") Then
                'MessageBox.Show("Esto es un comentario")
            ElseIf arrayRich(i).StartsWith("'") Then
                'MessageBox.Show("Esto es un comentario")
            ElseIf arrayRich(i).StartsWith("<!") Then
                'MessageBox.Show("Esto es un comentario")
            Else
                Dim texto As String = arrayRich(i)
                'hago un salto dentro del array cada vez que encuentro un delimitador
                vectorAux = texto.Split(delimitadores, StringSplitOptions.RemoveEmptyEntries)
                'aumento mi variable l=lineas para indicar en que linea está cada token


                l += 1
                ' MsgBox(texto)
                'itero cada item del vectorAux, acá parto las lineas por palabras(Tokens)
                'para posteriormente agregarlas al DGV
                For Each item As String In vectorAux
                    Try
                        dgvDatos.Rows.Add()
                        dgvDatos.Rows(c).Cells(1).Value = item
                        dgvDatos.Rows(c).Cells(0).Value = l
                        c = c + 1
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                Next
            End If
        Next

        l = 0
    End Sub
    Private Sub procesar()
        Try
            Dim linea As Integer = 0
            Dim contenido As String = ""
            Dim contenido2 As String = ""
            For Each fila As DataGridViewRow In dgvDatos.Rows
                fila.Cells(1).Value = fila.Cells(1).Value.Replace(" ", "")
                'coloque tostring
                contenido = fila.Cells(1).Value
                contenido2 = fila.Cells(1).Value.ToString

                'valido tokens
                For Each ItemValue As String In lbReservadas.Items
                    If contenido.ToLower = ItemValue.ToLower Then
                        ' MsgBox("RESERVADA")
                        dgvDatos.Rows(linea).Cells(2).Value = "X"
                        dgvDatos.Rows(linea).Cells(2).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(2).Style.ForeColor = Color.White
                        dgvDatos.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If
                Next


                If contenido2.ToLower.Contains("(") Or contenido2.ToLower.Contains(")") Then
                    'MsgBox(contenido2.ToLower)
                    dgvDatos.Rows(linea).Cells(2).Value = "X"
                    dgvDatos.Rows(linea).Cells(2).Style.BackColor = Color.Black
                    dgvDatos.Rows(linea).Cells(2).Style.ForeColor = Color.White
                    dgvDatos.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                End If


                For Each ItemValue As String In lbOperadores.Items
                    If contenido.ToLower = ItemValue.ToLower Then
                        dgvDatos.Rows(linea).Cells(4).Value = "X"
                        dgvDatos.Rows(linea).Cells(4).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(4).Style.ForeColor = Color.White
                        dgvDatos.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                        Dim operador As Integer = CInt(dgvDatos.Rows(linea).Index)
                        Dim variable As String = dgvDatos.Rows(operador - 1).Cells(1).Value.ToString
                        Dim variableD As String = dgvDatos.Rows(operador + 1).Cells(1).Value.ToString

                        'valida variable izquierda
                        If validarAbecedario(variable) = False Then
                            If IsNumeric(variable) Then
                            Else
                                dgvDatos.Rows(operador - 1).Cells(7).Value = "X"
                                dgvDatos.Rows(operador - 1).Cells(7).Style.BackColor = Color.Black
                                dgvDatos.Rows(operador - 1).Cells(7).Style.ForeColor = Color.White
                                dgvDatos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                                lbErrores.Items.Add("Error: Token, " & contenido & "no forma parte del alfabeto.")
                            End If
                        Else
                            dgvDatos.Rows(operador - 1).Cells(3).Value = "X"
                            dgvDatos.Rows(operador - 1).Cells(3).Style.BackColor = Color.Black
                            dgvDatos.Rows(operador - 1).Cells(3).Style.ForeColor = Color.White
                            dgvDatos.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        End If

                        'valida variable derecha
                        If validarAbecedario(variableD) = False Then
                            If IsNumeric(variableD) Then

                            Else
                                dgvDatos.Rows(operador + 1).Cells(7).Value = "X"
                                dgvDatos.Rows(operador + 1).Cells(7).Style.BackColor = Color.Black
                                dgvDatos.Rows(operador + 1).Cells(7).Style.ForeColor = Color.White
                                dgvDatos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                            End If
                        Else
                            dgvDatos.Rows(operador + 1).Cells(3).Value = "X"
                            dgvDatos.Rows(operador + 1).Cells(3).Style.BackColor = Color.Black
                            dgvDatos.Rows(operador + 1).Cells(3).Style.ForeColor = Color.White
                            dgvDatos.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        End If

                    End If
                Next

                'CELDA DE TIPO DE DATO
                If IsNumeric(contenido) Then
                    dgvDatos.Rows(linea).Cells(5).Value = "X"
                    dgvDatos.Rows(linea).Cells(5).Style.BackColor = Color.Black
                    dgvDatos.Rows(linea).Cells(5).Style.ForeColor = Color.White
                    dgvDatos.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    If contenido Mod 1 = 0 Then
                        dgvDatos.Rows(linea).Cells(8).Value = "ENTERO"
                        dgvDatos.Rows(linea).Cells(8).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(8).Style.ForeColor = Color.White
                        dgvDatos.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    Else
                        dgvDatos.Rows(linea).Cells(8).Value = "DECIMAL"
                        dgvDatos.Rows(linea).Cells(8).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(8).Style.ForeColor = Color.White
                        dgvDatos.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If

                End If

                For Each ItemValue As String In lbOp.Items
                    If contenido.ToLower = ItemValue.ToLower Then
                        dgvDatos.Rows(linea).Cells(6).Value = "X"
                        dgvDatos.Rows(linea).Cells(6).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(6).Style.ForeColor = Color.White
                        dgvDatos.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If
                Next


                If validarAbecedario(contenido) = False Then
                    dgvDatos.Rows(linea).Cells(7).Value = "X"
                    dgvDatos.Rows(linea).Cells(7).Style.BackColor = Color.Black
                    dgvDatos.Rows(linea).Cells(7).Style.ForeColor = Color.White
                    dgvDatos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    lbErrores.Items.Add("Error: Token, " & contenido & " no forma parte del alfabeto.")
                Else
                    dgvDatos.Rows(linea).Cells(3).Value = "X"
                    dgvDatos.Rows(linea).Cells(3).Style.BackColor = Color.Black
                    dgvDatos.Rows(linea).Cells(3).Style.ForeColor = Color.White
                    dgvDatos.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


                    If contenido.Length = 1 And Not IsNumeric(contenido) Then
                        dgvDatos.Rows(linea).Cells(8).Value = "CHAR(1)"
                        dgvDatos.Rows(linea).Cells(8).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(8).Style.ForeColor = Color.White
                        dgvDatos.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ElseIf contenido.Length > 1 And dgvDatos.Rows(linea).Cells(2).Value = "" Then
                        dgvDatos.Rows(linea).Cells(8).Value = "VARCHAR(" & contenido.Length & ")"
                        dgvDatos.Rows(linea).Cells(8).Style.BackColor = Color.Black
                        dgvDatos.Rows(linea).Cells(8).Style.ForeColor = Color.White
                        dgvDatos.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If



                    If dgvDatos.Rows(linea).Cells(2).Value = "X" Then
                        dgvDatos.Rows(linea).Cells(3).Value = ""
                        dgvDatos.Rows(linea).Cells(3).Style.BackColor = Color.White
                        dgvDatos.Rows(linea).Cells(3).Style.ForeColor = Color.White
                        dgvDatos.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    End If
                End If

                For celda As Integer = 2 To 6 Step 1
                    'MsgBox(dgvDatos.Rows(l).Cells(celda).Value)
                    If dgvDatos.Rows(linea).Cells(celda).Value = "X" Then
                        dgvDatos.Rows(linea).Cells(7).Value = ""
                        dgvDatos.Rows(linea).Cells(7).Style.BackColor = Color.White
                        dgvDatos.Rows(linea).Cells(7).Style.ForeColor = Color.White
                        dgvDatos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    End If
                Next


                linea += 1
            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)
        'dgvDatos.Rows.Clear()
        'l = 0
        'c = 0
        Try
            For Each fila As DataGridViewRow In dgvDatos.Rows
                For celda As Integer = 2 To 6 Step 1
                    'MsgBox(dgvDatos.Rows(l).Cells(celda).Value)
                    If dgvDatos.Rows(l).Cells(celda).Value > "" Then
                        dgvDatos.Rows(l).Cells(7).Value = ""
                        dgvDatos.Rows(l).Cells(7).Style.BackColor = Color.White
                        dgvDatos.Rows(l).Cells(7).Style.ForeColor = Color.White
                        dgvDatos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    End If
                Next
                l += 1
            Next
            l = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        lbErrores.Items.Clear()

        If dgvDatos.RowCount > 0 Then
            c = 0
            dgvDatos.Rows.Clear()
            agregarDatos()
            procesar()

        Else
            agregarDatos()
            procesar()
        End If

    End Sub

    Private Sub rcExpresionRegular_TextChanged(sender As Object, e As EventArgs) Handles rcExpresionRegular.TextChanged

    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Diccionario.Show()
    End Sub

    Private Sub lbReservadas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbReservadas.SelectedIndexChanged

    End Sub

    Private Sub lbReservadas_DoubleClick(sender As Object, e As EventArgs) Handles lbReservadas.DoubleClick
        Try
            Reservada.lbReservadas.Items.Clear()
            For Each item In lbReservadas.Items
                Reservada.lbReservadas.Items.Add(item)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Reservada.Show()
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class
