﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Diccionario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtReservada = New System.Windows.Forms.TextBox()
        Me.btnReservada = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtReservada
        '
        Me.txtReservada.BackColor = System.Drawing.Color.White
        Me.txtReservada.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReservada.Location = New System.Drawing.Point(12, 85)
        Me.txtReservada.Name = "txtReservada"
        Me.txtReservada.Size = New System.Drawing.Size(236, 30)
        Me.txtReservada.TabIndex = 14
        '
        'btnReservada
        '
        Me.btnReservada.BackColor = System.Drawing.Color.Teal
        Me.btnReservada.FlatAppearance.BorderSize = 0
        Me.btnReservada.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReservada.ForeColor = System.Drawing.Color.White
        Me.btnReservada.Location = New System.Drawing.Point(12, 121)
        Me.btnReservada.Name = "btnReservada"
        Me.btnReservada.Size = New System.Drawing.Size(236, 41)
        Me.btnReservada.TabIndex = 15
        Me.btnReservada.Text = "AGREGAR"
        Me.btnReservada.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(228, 2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(31, 31)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "X"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 17)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "AGREGAR PALABRA RESERVADA"
        '
        'Diccionario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(260, 174)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnReservada)
        Me.Controls.Add(Me.txtReservada)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Diccionario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Diccionario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtReservada As TextBox
    Friend WithEvents btnReservada As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
End Class
